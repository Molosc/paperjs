Package.describe({
    name: 'molosc:paperjs',
    version: '0.0.3',
    // Brief, one-line summary of the package.
    summary: 'Paperjs for Meteor',
    // URL to the Git repository containing the source code for this package.
    git: 'https://Molosc@bitbucket.org/Molosc/paperjs.git',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.2.1');
    api.use('ecmascript');
    api.addFiles(['paperjs.js'], 'client');
    api.export('paper', 'client');
});

Package.onTest(function(api) {
    api.use('ecmascript');
    api.use('tinytest');
    api.use('molosc:paperjs');
    api.addFiles('paperjs-tests.js');
});
